
# Adaptative Panes

Panels has always had a bit of a problem when working with responsive designs. This module provides an interface to configure panes to be distributed in a grid for each available breakpoints in the site, much like Bootstrap grid works.

## Defining Breakpoints

This module depends on the Breakpoints module to find media query information and other things.

### Custom breakpoints

To use custom breakpoints, please visit create breakpoints following the instructions provided by that module and then visit *admin/structure/panels/settings/adaptative-panes* to configure their usage. Keep in mind that by using custom breakpoint, you'll also have to create your own CSS and grid system. This can be made easier by using our grid builder tool made with Sass and available on the sass directory.

### Default breakpoints

By default we expect you are going to use only the breakpoints that can be create by the Adaptative Panes module. To create them, though, you have to visit the module administration page at *admin/structure/panels/settings/adaptative-panes* and click the *Create default breakpoints* button. After that, you are all set to start using the modules functionallity.
