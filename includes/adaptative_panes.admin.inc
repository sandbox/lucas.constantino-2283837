<?php

/**
 * @file
 * Adaptative Panes admin interface.
 */

/**
 * Form callback for adaptative panes configuration.
 */
function adaptative_panes_admin_settings_page($form, &$form_state) {

  $breakpoints = breakpoints_breakpoint_load_all();
  $adaptative_panes_default_breakpoints = _breakpoints_breakpoint_load_all_by_type('custom', 'adaptative_panes');

  // Inform user that no breakpoint is currently set.
  if (empty($breakpoints)) {
    $message  = 'There is no breakpoint currently registered. ';
    $message += 'Please create some breakpoints before you can configure ';
    $message += 'adaptative pane settings, as breakpoints are a dependency.';
    drupal_set_message(t($message), 'warning');
    return system_settings_form($form);
  }

  // Make customized settings.
  $form['use_defaults'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use default settings'),
    '#description' => t('To use out-of-the box setting, leave this option checked.'),
  );

  $customized_state = array(
    'visible' => array(
      ':input[name="use_defaults"]' => array('checked' => FALSE),
    ),
  );

  // Load default css.
  $form['default_css'] = array(
    '#type' => 'checkbox',
    '#title' => t('Load default CSS'),
    '#description' => t('Load the default stylesheet that adresses the default
      breakpoints.'),
    '#states' => $customized_state,
  );

  // Column related settings.
  $form['columns'] = array(
    '#type' => 'fieldset',
    '#title' => t('Column settings'),
    '#tree' => TRUE,
    '#states' => $customized_state,
  );

  // Column amount.
  $form['columns']['amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Column amount'),
    '#description' => t('How many columns will the grid system have?'),
    '#size' => 20,
    '#required' => TRUE,
  );

  // Allow for shifting.
  $form['columns']['shifting'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow shifting'),
    '#description' => t('Allow the definition of a shifting that will drag the
      column forward or backards.'),
  );

  // Allow for bleeding.
  $form['columns']['bleeding'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow bleeding'),
    '#description' => t("Allow users to configure a pane to have it's contents
          bleed to the edges of this pane, ignoring the default column padding."),
  );

  // Breakpoint related settings.
  $form['breakpoints'] = array(
    '#type' => 'fieldset',
    '#title' => t('Breakpoint settings'),
    '#tree' => TRUE,
    '#states' => $customized_state,
  );

  $allowed_description  = 'Choose what kind of breakpoints should configuration ';
  $allowed_description += 'be presented for when configuring adaptative settings ';
  $allowed_description += 'for a pane.';

  // Allowed breakpoint.
  $form['breakpoints']['allowed_breakpoints'] = array(
    '#type' => 'select',
    '#title' => t('What breakpoints should be available?'),
    '#description' => t($allowed_description),
    '#options' => array(
      'adaptative_panes' => t('Adapative Panes defaults'),
      'theme' => t('From current theme'),
      'theme_parents' => t('From current theme and its parents'),
      'all' => t('All'),
      'select' => t('Choose from the list...'),
    ),
  );

  // Filter by active breakpoints.
  $form['breakpoints']['filter_by_active_breakpoints'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use only active breakpoints'),
  );

  $state = array(
    ':input[name="breakpoints[allowed_breakpoints]"]' => array('value' => 'select'),
  );

  // Custom selection of allowed breakpoints.
  $form['breakpoints']['allowed_breakpoints_select'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select which breakpoints should be available:'),
    '#options' => array(),
    '#states' => array(
      'visible' => $state,
      'required' => $state,
    ),
  );

  $options = &$form['breakpoints']['allowed_breakpoints_select']['#options'];

  // Add breakpoint options for a custom select.
  foreach ($breakpoints as $name => $breakpoint) {
    $options[$name] = $breakpoint->name;
  }

  // Set defaults, if available.
  $current_config = variable_get('adaptative_panes_configuration');
  adaptative_panes_set_default_form_values($current_config, $form);

  // Form actions.
  $form['actions']['#type'] = 'actions';

  // Default configuration saver.
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
    '#submit' => array('adaptative_panes_admin_settings_page_submit'),
  );

  if (empty($adaptative_panes_default_breakpoints)) {
    // Create breakpoints button.
    $form['actions']['create_breakpoints'] = array(
      '#type' => 'submit',
      '#value' => t('Create default breakpoints'),
      '#submit' => array('adaptative_panes_admin_create_breakpoints_submit'),
    );
  }
  else {
    $form['actions']['remove_breakpoints'] = array(
      '#type' => 'link',
      '#title' => t('Remove default breakpoints'),
      '#href' => 'admin/structure/panels/settings/adaptative-panes/delete-default-breakpoints',
      '#options' => array(
        'attributes' => array(
          'class' => array('button'),
        ),
      ),
    );
  }

  return $form;
}

/**
 * Adaptative panes configuration form submit handler.
 */
function adaptative_panes_admin_settings_page_submit($form, $form_state) {

  $settings = array();

  // Exclude unnecessary elements.
  form_state_values_clean($form_state);

  // Reset to defaults when use defaults is checked.
  if (!empty($form_state['values']['use_defaults'])) {
    $settings = variable_get('adaptative_panes_configuration_defaults');
  }
  // Fulfill settings with each of the form's options.
  else {
    foreach ($form_state['values'] as $key => $value) {
      if (is_array($value) && isset($form_state['values']['array_filter'])) {
        $value = array_keys(array_filter($value));
      }
      $settings[$key] = $value;
    }
  }

  drupal_set_message(t('The adaptative options have been saved.'));

  // Save all settings.
  variable_set('adaptative_panes_configuration', $settings);
}

/**
 * Create default breakpoints form submit handler.
 */
function adaptative_panes_admin_create_breakpoints_submit($form, $form_state) {

  $breakpoint_group = breakpoints_breakpoint_group_load('adaptative_panes');
  $breakpoints = _breakpoints_breakpoint_load_all_by_type('custom', 'adaptative_panes');

  // Create group, if not already available.
  if (!$breakpoint_group) {
    $breakpoint_group = breakpoints_breakpoint_group_empty_object();
    $breakpoint_group->machine_name = 'adaptative_panes';
    $breakpoint_group->name = 'Adaptative Panes Breakpoints';
  }

  // Create breakpoints, if not already available.
  if (empty($breakpoints)) {

    ctools_include('export');

    // Default breakpoint information.
    $new_breakpoints = array(
      'tiny' => array(
        'name' => 'Tiny',
        'breakpoint' => 'screen and (max-width: 479px)',
      ),
      'extra_small' => array(
        'name' => 'Extra Small',
        'breakpoint' => 'screen and (min-width: 480px) and (max-width: 767px)',
      ),
      'small' => array(
        'name' => 'Small',
        'breakpoint' => 'screen and (min-width: 768px) and (max-width: 991px)',
      ),
      'medium' => array(
        'name' => 'Medium',
        'breakpoint' => 'screen and (min-width: 992px) and (max-width: 1199px)',
      ),
      'large' => array(
        'name' => 'Large',
        'breakpoint' => 'screen and (min-width: 1200px)',
      ),
    );

    foreach ($new_breakpoints as $machine_name => $new_breakpoint) {
      $breakpoint               = breakpoints_breakpoint_empty_object();
      $breakpoint->name         = $new_breakpoint['name'];
      $breakpoint->machine_name = $machine_name;
      $breakpoint->breakpoint   = $new_breakpoint['breakpoint'];
      $breakpoint->source       = 'adaptative_panes';
      $breakpoint->source_type  = 'custom';

      breakpoints_breakpoint_save($breakpoint);

      // Add breakpoint to the group.
      $breakpoint_group->breakpoints[] = $machine_name;
    }

    drupal_set_message(t('Default breakpoints have been created.'));
  }
  else {

    // Inform user that there were already default breakpoints.
    drupal_set_message(t('Default breakpoints were already defined.'), 'warning');
  }

  // Save changes to breakpoint group.
  breakpoints_breakpoint_group_save($breakpoint_group);
}

/**
 * Remove default breakpoints form confirmation.
 */
function adaptative_panes_admin_remove_breakpoints_confirm_form($form, $form_state) {

  // Set confirmation submition.
  $form['#submit'] = array('adaptative_panes_admin_remove_breakpoints_confirm_form_submit');

  $question = t('Are you sure you want to delete the default breakpoints?');
  $description = t('After deleting the default breakpoints, any current 
    configuration on these breakpoints will be lost.');
  $cancel_path = 'admin/structure/panels/settings/adaptative-panes';

  return confirm_form($form, $question, $cancel_path, $description, t('Delete'), t('Cancel'));
}

/**
 * Remove default breakpoints form submit handler.
 */
function adaptative_panes_admin_remove_breakpoints_confirm_form_submit($form, &$form_state) {

  adaptative_panes_remove_default_breakpoints();

  // Send user to main configuration page.
  $form_state['redirect'] = 'admin/structure/panels/settings/adaptative-panes';

  drupal_set_message(t('Removed default breakpoints.'));
}
