<?php

/**
 * @file
 * Form related code.
 */

/**
 * Settings form callback.
 *
 * Create the setting form displayed in the modal popup when configuring
 * adaptative pane settings.
 */
function adaptative_panes_pane_settings_form($style_settings, &$form_state) {

  $pid = $form_state['pid'];

  // Create the adaptative settings form.
  $form = adaptative_panes_create_adaptative_settings_form($pid);

  // We'll need a way to find pane information when saving the form.
  $form['pid'] = array(
    '#type' => 'hidden',
    '#value' => $pid,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Form submit for the settings form.
 */
function adaptative_panes_pane_settings_form_submit(&$form, &$form_state) {

  $adaptative_settings = array();
  $breakpoints = adaptative_panes_get_available_breakpoints();

  // Find and map data in the form for each of the available breakpoints.
  foreach ($breakpoints as $machine_name => $breakpoint) {

    // There is a parsing on the form element names when the form is submited
    // that converts many characters in underscores. To keep all the things
    // normalized, we also make this replacement when creating the form data,
    // so that we can then always reference the settins using the same name
    // everywere.
    $settings = $form_state['values'][str_replace('.', '_', $machine_name)];

    if (!empty($settings)) {
      $adaptative_settings[$machine_name] = $settings;
    }
  }

  // Create a temporary variable with adaptative settings for the current
  // pane. This variable will be read both upon saving the display with IPE or
  // when reentering the adaptative settings form for this same pane.
  variable_set('adaptative_pane_settings_' . $form_state['pid'], $adaptative_settings);
}

/**
 * Implements hook_form_alter().
 */
function adaptative_panes_form_panels_ipe_edit_control_form_alter(&$form, &$form_state, $form_id) {
  // Change the order of submit functions.
  // The adaptative_panes_ipe_form_submit passes the upgraded pane to the
  // form_state.
  array_unshift($form['buttons']['submit']['#submit'], 'adaptative_panes_ipe_edit_control_form_submit');
}

/**
 * Custom submit function for the form panels_ipe_edit_control_form().
 */
function adaptative_panes_ipe_edit_control_form_submit(&$form, &$form_state) {

  foreach ($form_state['display']->content as $pid => &$pane) {
    $adaptative_settings = variable_get('adaptative_pane_settings_' . $pid);
    variable_del('adaptative_pane_settings_' . $pid);

    // Save settings to the display handler, as IPE will later on save this
    // display data and consequently save it's panes data.
    $pane->configuration['adaptative_pane'] = $adaptative_settings;
  }
}
