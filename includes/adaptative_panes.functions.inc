<?php

/**
 * @file
 * Module's helper functions.
 */

/**
 * Helper method to load panels pane data by id.
 *
 * @param int|string $pid
 *   Numeric identification for the pane to load.
 * @param array $fields
 *   Optional filter of fields to return.
 *
 * @return array
 *   Associative array with pane data.
 */
function adaptative_panes_pane_load($pid, array $fields = array()) {

  $query = db_select('panels_pane', 'pp')
    ->condition('pid', $pid, '=');

  if (!empty($fields)) {
    $query->fields('pp', $fields);
  }

  $pane = $query->execute()->fetchAssoc();

  // As pane configuration are always stored in a serialized format, we make
  // them easier to work with by deserializing it here.
  if (array_search('configuration', $fields) && !empty($pane['configuration'])) {
    $pane['configuration'] = unserialize($pane['configuration']);
  }

  return $pane;
}

/**
 * Helper to crete the adaptative settings form for a given pane id.
 *
 * @param int|string $pid
 *   Numeric identification for the pane to get current configuration.
 *
 * @return array
 *   A renderable array for the adaptative settings of a pane.
 */
function adaptative_panes_create_adaptative_settings_form($pid) {

  $form = array();
  $breakpoints = adaptative_panes_get_available_breakpoints();
  $module_settings = variable_get('adaptative_panes_configuration');

  // Parse module settings.
  $columns = $module_settings['columns']['amount'];
  $allow_shifting = !empty($module_settings['columns']['shifting']);
  $allow_bleeding = !empty($module_settings['columns']['bleeding']);

  // The user might have submitted this same form before, but not finished his
  // work on IPE. In this case, this temporary variable will be available.
  $current_config = variable_get('adaptative_pane_settings_' . $pid);

  // When creating a new pane, "pid" comes as a string (usually "new-1"). When
  // working with a pre-existing pane, "pid" comes as a numeric value. In the
  // second case, we might try to fetch current configuration from the actual
  // saved pane.
  if (empty($current_config) && is_numeric($pid)) {
    $pane = adaptative_panes_pane_load($pid, array('pid', 'configuration'));
    $current_config = $pane['configuration']['adaptative_pane'];
  }

  // All settings are created dynamically based on the available breakpoints.
  foreach ($breakpoints as $machine_name => $breakpoint) {

    // Create breakpoint fieldset.
    $breakpoint_options = array(
      '#type' => 'fieldset',
      '#title' => "{$breakpoint->name} breakpoint: <i>{$breakpoint->breakpoint}</i>",
      '#tree' => TRUE,
    );

    // For the sake of code reading, set this long text before it's usage.
    $columns_desc = t('Set how many columns (of %columns) it will occupy.', array(
      '%columns' => $columns,
    )) . '<br />' . t('Choose "%columns" to occupy all available space.', array(
      '%columns' => $columns,
    ));

    // Create columns field.
    // @todo: make the column amount configurable.
    $breakpoint_options['columns'] = array(
      '#type' => 'select',
      '#title' => t('Columns'),
      '#description' => $columns_desc,
      '#options' => array(
        '<none>' => t('Hidden'),
      ),
      '#default_value' => $columns,
    );

    // Add column options.
    for ($c = 1; $c <= $columns; $c++) {
      $breakpoint_options['columns']['#options'][$c] = format_plural($c, '1 column', '@count columns');
    }

    // Add bleeding option.
    if ($allow_bleeding) {
      $breakpoint_options['bleed'] = array(
        '#type' => 'checkbox',
        '#title' => t('Bleed content to the margins.'),
        '#default_value' => FALSE,
      );
    }

    // Add shift option group and fields.
    if ($allow_shifting) {
      $breakpoint_options['shift'] = array(
        '#type' => 'fieldset',
        '#title' => 'Shifting options',
        '#tree' => TRUE,
        '#collapsed' => TRUE,
        '#collapsible' => TRUE,
        'left' => array(
          '#type' => 'select',
          '#title' => t('Left'),
          '#description' => t('Choose how many columns should be shifted %position the pane.', array(
            '%position' => 'before',
          )),
          '#default_value' => '<none>',
        ),
        'right' => array(
          '#type' => 'select',
          '#title' => t('Right'),
          '#description' => t('Choose how many columns should be shifted %position the pane.', array(
            '%position' => 'after',
          )),
          '#default_value' => '<none>',
        ),
      );

      // Add shift options.
      for ($c = -$columns; $c <= $columns; $c++) {
        if ($c != 0) {
          $label = format_plural($c, '1 column', '@count columns');
          $value = $c;
        }
        else {
          $label = t('None');
          $value = '<none>';
        }

        $breakpoint_options['shift']['left']['#options'][$value] = $label;
        $breakpoint_options['shift']['right']['#options'][$value] = $label;
      }
    }

    // Set default values based on current configuration, if available.
    if (!empty($current_config[$machine_name])) {

      // Set current values recursively.
      adaptative_panes_set_default_form_values($current_config[$machine_name], $breakpoint_options);

      // Uncollapse shift fieldset when there are values.
      if ($allow_shifting && $breakpoint_options['shift']['left']['#default_value'] != '<none>' || $breakpoint_options['shift']['right']['#default_value'] != '<none>') {
        $breakpoint_options['shift']['#collapsed'] = FALSE;
      }
    }

    // There is a parsing on the form element names when the form is submited
    // that converts many characters in underscores. To keep all the things
    // normalized, we also make this replacement before the need of it, so we
    // can always reference the settins using the same name everywere.
    $form[str_replace('.', '_', $machine_name)] = $breakpoint_options;
  }

  return $form;
}

/**
 * Set default values on a form based on settings configuration array.
 *
 * @param array $config
 *   The current configuration as an associative array.
 * @param array $form
 *   A renderable form.
 */
function adaptative_panes_set_default_form_values(array $config, array &$form) {
  foreach ($config as $name => $value) {
    if (!empty($form[$name])) {
      if (is_array($value) && $form[$name]['#type'] == 'checkboxes') {
        $form[$name]['#default_value'] = array_keys(array_filter($value));
      }
      elseif (is_array($value)) {
        adaptative_panes_set_default_form_values($config[$name], $form[$name]);
      }
      else {
        $form[$name]['#default_value'] = $value;
      }
    }
  }
}

/**
 * Checks if there are breakpoints available.
 *
 * @param array $options
 *   Configuration to override the settings variable.
 *
 * @return bool
 *   Whetere or not tehre are available breakpoints.
 */
function adaptative_panes_has_available_breakpoints(array $options = array()) {
  $breakpoints = adaptative_panes_get_available_breakpoints($options);
  return !empty($breakpoints);
}

/**
 * Get the available breakpoints as per configuration.
 *
 * @param array $options
 *   Configuration to override the settings variable.
 *
 * @return array
 *   All available breakpoints as machine name => breakpoint info.
 */
function adaptative_panes_get_available_breakpoints(array $options = array()) {

  $theme = variable_get('theme_default');
  $settings = variable_get('adaptative_panes_configuration');

  // Default handler.
  if (empty($settings['breakpoints'])) {
    return breakpoints_breakpoint_load_all_active();
  }

  // Init the returning variable.
  $breakpoints = array();

  // Load breakpoints based on settings definition.
  switch ($settings['breakpoints']['allowed_breakpoints']) {
    case 'adaptative_panes':
      $breakpoints = _breakpoints_breakpoint_load_all_by_type('custom', 'adaptative_panes');
      break;

    case 'theme':
      $breakpoints = breakpoints_breakpoint_load_all_theme($theme);
      break;

    case 'theme_parents':
      $themes = system_list('theme');
      $breakpoints = breakpoints_breakpoint_load_all_theme($theme);

      if (isset($themes[$theme]->base_themes)) {
        foreach ($themes[$theme]->base_themes as $key => $theme_name) {
          $breakpoints += breakpoints_breakpoint_load_all_theme($key);
        }
      }
      break;

    case 'all':
      $breakpoints = breakpoints_breakpoint_load_all();
      break;

    case 'select':
      $breakpoints = breakpoints_breakpoint_load_all();
      $selection = $settings['breakpoints']['allowed_breakpoints_select'];

      // Filter not selected breakpoints.
      foreach ($breakpoints as $machine_name => $breakpoint) {
        if (empty($selection[$machine_name])) {
          unset($breakpoints[$machine_name]);
        }
      }
      break;
  }

  // Filter by active breakpoints, if necessary.
  if (!empty($settings['breakpoints']['filter_by_active_breakpoints'])) {
    foreach ($breakpoints as $machine_name => $breakpoint) {
      if (!$breakpoint || !$breakpoint->status) {
        unset($breakpoints[$machine_name]);
      }
    }
  }

  // @todo this function's return should be cached out.
  return $breakpoints;
}

/**
 * Remove all breakpoints that were created by this module.
 */
function adaptative_panes_remove_default_breakpoints() {

  $breakpoint_group = breakpoints_breakpoint_group_load('adaptative_panes');
  $breakpoints = _breakpoints_breakpoint_load_all_by_type('custom', 'adaptative_panes');

  // Remove breakpoint group.
  if ($breakpoint_group) {
    breakpoints_breakpoint_group_delete($breakpoint_group);
  }

  // If no breakpoints were available, inform user.
  if (empty($breakpoints)) {
    drupal_set_message(t('There were no breakpoints to be deleted.'), 'warning');
    return;
  }

  // Delete each breakpoint.
  foreach ($breakpoints as $breakpoint) {
    breakpoints_breakpoint_delete($breakpoint->machine_name);
  }
}
