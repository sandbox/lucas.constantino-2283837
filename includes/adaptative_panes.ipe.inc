<?php

/**
 * @file
 * Code for integrating to IPE's interface.
 */

/**
 * Implements hook_panels_ipe_pane_links_alter().
 *
 * Add a adaptative configuration button to IPE bar on each pane.
 */
function adaptative_panes_panels_ipe_pane_links_alter(&$links, $context, $display) {

  // In case there is no breakpoint available or there is no pane information,
  // it doesn't make sense to add the configuration button.
  if (empty($context['pane']->pid) || !adaptative_panes_has_available_breakpoints()) {
    return;
  }

  $links['adaptative-settings'] = array(
    'title' => '<span>' . t('Adaptative Settings') . '</span>',
    'html' => TRUE,
    'href' => 'adaptative_panes/nojs/settings/form/' . $context['pane']->pid . '/' . 'node_view',
    'attributes' => array(
      'title' => t('Adaptative Settings'),
      'class' => 'ctools-use-modal ctools-modal-modal-popup-small',
    ),
  );

  drupal_add_css(drupal_get_path('module', 'adaptative_panes') . '/css/adaptative-panes-ipe.css');
}
