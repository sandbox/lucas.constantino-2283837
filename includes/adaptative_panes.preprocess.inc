<?php

/**
 * @file
 * Code for the Custom Adaptative Panels Preprocess functions.
 */

/**
 * Implements hook_preprocess_panels_pane().
 */
function adaptative_panes_preprocess_panels_pane(&$vars) {

  $pane = $vars['pane'];
  $pane_config = $pane->configuration['adaptative_pane'];
  $breakpoints = adaptative_panes_get_available_breakpoints();

  // Set pane breakpoint appearance settings.
  foreach ($breakpoints as $machine_name => $breakpoint) {

    // Skip breakpoint if no settings is available.
    // @todo: consider setting default values here.
    if (empty($pane_config[$machine_name])) {
      continue;
    }

    // Get settings for this breakpoint.
    $settings = $pane_config[$machine_name];

    $breakpoint_name_parts = explode('.', $machine_name);
    $breakpoint_clean_name = str_replace('_', '-', array_pop($breakpoint_name_parts));

    // Column width.
    $columns = $settings['columns'] == '<none>' ? 'hidden' : $settings['columns'];
    $vars['classes_array'][] = $breakpoint_clean_name . '-' . $columns;

    // Bleed option.
    if (!empty($settings['bleed'])) {
      $vars['classes_array'][] = $breakpoint_clean_name . '-bleed';
    }

    // Left/Right pulling.
    if (!empty($settings['shift'])) {
      foreach ($settings['shift'] as $side => $columns) {
        if ($columns != '<none>') {
          $vars['classes_array'][] = str_replace('_', '-', $breakpoint_clean_name . '-' . $side . '-' . $columns);
        }
      }
    }
  }
}

/**
 * Load default grid system CSS.
 */
function adaptative_panes_panels_pre_render($panels_display, $renderer) {

  $settings = variable_get('adaptative_panes_configuration');
  $module_path = drupal_get_path('module', 'adaptative_panes');

  if (!empty($settings['default_css'])) {
    drupal_add_css($module_path . '/css/adaptative-panes-default-grid.css');
  }
}
